﻿1. ¿Cuál de estos operadores no es un operador aritmético?

4. //

2. ¿Que es un tipo de dato?

1. Es un atributo de los datos que indica al ordenador sobre la clase de datos que se va
a manejar.

3. ¿Que tendría que retornar la siguiente operación?
“7”+”7”

c. “77”

4. ¿Qué es la concatenación en programación?

d. Todas son correctas

5. ¿Qué valor retorna esta premisa?
true and true

a. true

6. ¿Qué valor retorna esta premisa?
(true && true && false)

b. false

7. ((true || false) && true) && false

b. false

8. true || false && true

a. true

9. ¿Cuáles de estos operadores no es un operador relacional?

d. =!

10. ¿Cuál es la diferencia entre = y == ?

b. El primero se usa para asignar un valor y el segundo para comparar dos valores.

11. ¿Qué es un DDF?
a. Un diagrama de flujo es un diagrama que describe un proceso, sistema o algoritmo
informático

12. ¿Qué diferencia hay entre la memoria RAM y ROM?

b. La memoria RAM es una memoria volátil y la ROM es una memoria no volátil

13. ¿Que es un lenguaje de programación de alto nivel?

a. Son lenguajes que ejecutan sus algoritmos teniendo en cuenta las capacidades
cognitivas de los seres humanos y no teniendo en cuenta las capacidades de los
ordenadores de ejecutar las órdenes
