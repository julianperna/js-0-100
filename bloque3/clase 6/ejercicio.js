// 1. Cargar el nombre y apellido dentro de dos inputs type = "text" y al apretar un boton mostrar la concatenacion de estos strings en un tercer input de tipo text

// 2. Replicar la calculadora (+, -) y que una vez resuelto el calculo pasados 2 segundos limpiar los inputs

const MostrarInformacion = () => {
    let textConcat = document.getElementById('resultado');
    
    const nombre = document.getElementById('nombre').value;
    const apellido = document.getElementById('apellido').value;
    const resultado = `${nombre} ${apellido}`;
  
    return textConcat.value = resultado;
  };

  const reset = () => {
    document.getElementById('num1').value = null;  
    document.getElementById('num2').value = null;
    document.getElementById('totalOp').value = null;
}

const calcular = (op) => {
    let res = document.getElementById('totalOp');
    let num1 = parseInt(document.getElementById('num1').value) || 0;  
    let num2 = parseInt(document.getElementById('num2').value) || 0;
    res.value = (op === '+' ? num1 + num2 : num1 - num2);
    setTimeout(() => reset(), 2000)
};